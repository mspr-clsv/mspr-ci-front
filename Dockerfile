# STEP 1 build
FROM node:11-alpine as build-stage
WORKDIR /app
ARG BACKEND_URL
ENV VUE_APP_BACKEND_URL ${BACKEND_URL}
ARG HTTP_OR_HTTPS
ENV VUE_APP_HTTP_OR_HTTPS ${HTTP_OR_HTTPS}
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# STEP 2 deploy
FROM nginx:1.15-alpine
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY nginx/nginx.conf /usr/src/nginx.conf
EXPOSE $PORT
CMD envsubst '${PORT}' < /usr/src/nginx.conf > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'
