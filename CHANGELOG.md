## [0.4.0](https://gitlab.com/mspr-clsv/mspr-ci-front/compare/0.3.1...0.4.0) - 14 February 2020

### Merged

- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Fix commit icon on changelog [`#40`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/40)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> add perf test for master [`#39`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/39)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Adding commit svg icon [`#37`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/37)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Feature/webdiver io [`#35`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/35)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Changing img in changelog [`#34`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/34)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Adding tests end to end in package.json [`#33`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/33)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> add prod and preprod environnment [`#32`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/32)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Feature/webdiver io [`#19`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/19)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> update dockerfile [`#29`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/29)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Feature/rename stage pipeline [`#25`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/25)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Adding badge for renovate [`#23`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/23)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> 4.0.0 application version [`#24`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/24)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Merging fix [`#22`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/22)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Renovate config [`#21`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/21)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Develop [`#18`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/18)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Feature/update deploy heroku [`#17`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/17)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Feature/deploy heroku [`#16`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/16)

### Commits

- <img src = "./ressources/commit.svg" width="32px" height="32px"/> merge develop into feature [`add9aed`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/add9aedeeeb6ac2e2b62e082d766c1f107c4a37c)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> deployment auto OK and clean project [`e56ff03`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/e56ff03d856174d9a8185946cc45f6d6fc76e703)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> merge develop into feature [`c504dd5`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/c504dd5587078c3b2ab48d0dbfcc58fc416706ec)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding commit and merge img [`519480a`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/519480a9ac7944c52acbad9ed3275456b94ac525)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> WIP - Added more tests + refactored scripts [`7521923`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/7521923006bd4b71b9accb7bea747151db4c201e)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> fix icon commit svg [`10dddaa`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/10dddaa29ed529f3adf31688f50630a444b29962)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> add coverage files [`9cf0b9c`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/9cf0b9cff408be45dc29b20f8e6fb2febf5582ce)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> added environnement variable [`6bf3b1f`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/6bf3b1fcc987917aef4e7cfaba37e64eb879c003)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Added report [`7dfdf44`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/7dfdf447a8689aa30c29d1150aaa10526c21318e)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> updating readme [`a4a787b`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/a4a787b2d87b12019deb9ed7b86792df85519544)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> added coverage report [`4b6c340`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/4b6c340ef45930f8dfdd053671678c5e82726549)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> merge develop into feature [`e12091f`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/e12091f830c50fa65fc7d71f746edc03796b4b80)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> deploy heroku only master [`e754802`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/e7548022631d455cf280d0ecea294ae4d4b48844)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> update coverage [`a495f09`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/a495f09abd6285030a545e6e40e4e4fee6a9c03d)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> removing renovate [`66811e7`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/66811e7a77b58bd6b479756e878030ac2b42d09c)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> switch perf test from master to develop [`7d8fd4e`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/7d8fd4e593becdb3d6cca2ec82bdd3d223a90cd8)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> resolving conflits [`ed83c9d`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/ed83c9d55a66768be0d6b3d9170abba135bbad9d)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Merge dev into feature [`09074ad`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/09074adef7bf0ee463a4cc76f9025c26a384f2f6)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> merge develop into feature [`7fa2049`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/7fa204905dfbc28d48d42118c03c52d4b079994d)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> fix merge error [`dc737df`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/dc737df98902ffd6cc12e4017773a112933a67ff)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> badge test in the readme.md [`353c115`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/353c115455e176b2d75dc5dcc5591f2eb3fef372)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> upgrading application version [`220ad0a`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/220ad0aff9953fa28dbfaaa3806958211f11c056)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding config.js for renovate [`59a2a54`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/59a2a54228524b77fbd858a0818fb92f7b66560c)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding renovate to gitlab.ci [`9ae2e12`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/9ae2e129933f001b0176e74623db3018ca4fe29b)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> upgrading application version [`c2982c2`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/c2982c28a731503169ef764b12ddd44e5cd150a5)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Started article removal of cart [`2bfa530`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/2bfa530fa15dc23c7dcedf433d44af11191e0188)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding config.js for renovate [`59acfe7`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/59acfe7dcc7944b7471dcee8f229bcf56342f977)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding redirection path for renovate badge [`59d5925`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/59d5925640cdaa3c0791aa56261f5605d459b1b9)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding section in readme for renovate badge [`936846e`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/936846e65b2f57b0030bebccbface3747d529478)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> merge develop into feature [`cdb1681`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/cdb168173f7a9b1f2018c23f0a422703ca8e3362)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Add renovate.json [`97e6875`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/97e6875d2694a2957d13da038d0067f1954a19c2)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> fix gitlab ci [`bf0c167`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/bf0c167ee50d418fafb574792993ec8e7584c31c)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> fix gitlab ci [`6a6089b`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/6a6089ba6b610b1980c01ca30b899614f34f4201)

## [0.3.1](https://gitlab.com/mspr-clsv/mspr-ci-front/compare/0.3.0...0.3.1) - 13 February 2020

### Merged

- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Adding unit tests and some badges [`#14`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/14)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Develop [`#13`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/13)

### Commits

- <img src = "./ressources/commit.svg" width="32px" height="32px"/> init [`373cc3d`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/373cc3d79c67599c9e37010c4d87046fd7f63e61)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> WIP Added client page tests [`2fd2a33`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/2fd2a336e5d9ddfbc1e286e4d0490ca0db751089)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> deployment ok [`2e9c348`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/2e9c348572c4622bf3cb70b8ed16e222acf5c0fd)

## [0.3.0](https://gitlab.com/mspr-clsv/mspr-ci-front/compare/0.2.2...0.3.0) - 11 February 2020

### Merged

- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Feature/gitlab ci [`#12`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/12)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Automatic Changelog : OK [`#11`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/11)

### Commits

- <img src = "./ressources/commit.svg" width="32px" height="32px"/> test on new icon [`63060d0`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/63060d06d57f0829334237b826ce3b8f7f5b4753)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding new icon [`08d98f2`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/08d98f2011e458e89cd8c31c05519b5699d3215d)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> add gitlab-ci [`37f4cd4`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/37f4cd4e3cbfe465e6d2989d7fe371281ae14553)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Adding € symbol [`8211a03`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/8211a0321f6331929bb160b02511e848bf207405)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> fix gitlab-ci [`180271e`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/180271e493ee3c412b5c04e87ac5be425b6209d0)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> fix gitlab-ci #2 [`e4a6306`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/e4a630618a07356f76985877beebd0cf050a48df)

## [0.2.2](https://gitlab.com/mspr-clsv/mspr-ci-front/compare/0.2.1...0.2.2) - 11 February 2020

### Commits

- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Test upgrade [`8f880c9`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/8f880c93f50a4e885cb7549cf133001f9981891e)

## 0.2.1 - 11 February 2020

### Merged

- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Adding changelog automation [`#10`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/10)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Add changelog [`#9`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/9)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Feature/add product to purchase [`#8`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/8)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Master [`#7`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/7)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Update README.md [`#6`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/6)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Update client list UI [`#5`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/5)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> Added form to add client [`#4`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/4)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> add client list [`#3`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/3)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> add page products list [`#2`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/2)
- <img src = "./ressources/merge.svg" width="32px" height="32px"/> add router [`#1`](https://gitlab.com/mspr-clsv/mspr-ci-front/merge_requests/1)

### Commits

- <img src = "./ressources/commit.svg" width="32px" height="32px"/> first commit [`f067b7e`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/f067b7ef3f438f1b614dd6ce0293ecd61ba8a2f7)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Adding automation test [`340c406`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/340c40698173f681293aea90dba74dd593df3103)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> WIP: Billing [`2343a71`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/2343a71141acff3ff155fbe3003bd5a9f1a30762)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> WIP [`e6b0678`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/e6b0678d2b5788c273fac9d04cd77c598b07f51e)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Adding automation for changelog [`5e923b5`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/5e923b5cc2bc052b500ae64558fcc7e4820ffc16)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> fix husky -&gt; adding --save-dev [`7c87a1c`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/7c87a1c4506e51a52dae5b5320d6b7394bd93820)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> add topbar [`67bd0f2`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/67bd0f236bc9742bd08d65bededebbdf43ab2f9b)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> modifiyng changelog template [`a3ca8fc`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/a3ca8fcc6856894000f31bc413abe2967e7bdc07)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding template for changelog [`e996f4c`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/e996f4c78689e43d6c82719f9838b56e833e5b2a)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> format number [`a356d20`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/a356d203b6a033463197abc26e2ca2cce8025e4b)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> upgrading version to 0.2.0 [`43eb822`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/43eb8221496eb28f2a22b05a004416a50b879b9d)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> adding linter config [`bdb88f3`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/bdb88f32f3638b6de35abd205e614d6da6abee5b)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> modifiyng isoDate to niceDate [`6ca3fe1`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/6ca3fe1556df9fb9d0696a4a40beda9e1f8ff75a)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> upgrading version [`d73474b`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/d73474b19fd239d6aed9f02ffab4c26c60736901)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> upgrading version to 0.2.0 bis [`064e321`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/064e321b46f0f62b0d421c168044f7912913d936)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> New changelog [`826035b`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/826035bb99cb0fe192839a9facbb116bc8614688)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> test fixed + npm run version added bis [`f2e0805`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/f2e08056d4c5217b56f3d4806480809eb3472e24)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> upgrading version [`ca4000d`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/ca4000d1fe229e0f6cba18d6968ecc3def124173)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> Initial commit [`ea22f3b`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/ea22f3b23324f55b0cea3009b3ac4633ae593b2b)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> test fixed + npm run version added [`5d5d424`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/5d5d424259a231b1426c59bb260287a7685e9770)
- <img src = "./ressources/commit.svg" width="32px" height="32px"/> test without lint staged [`318822e`](https://gitlab.com/mspr-clsv/mspr-ci-front/commit/318822ef4175af047efeb01e5cd88d69624234e4)
