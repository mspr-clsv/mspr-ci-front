
module.exports = {
    collectCoverage: true,
    collectCoverageFrom: [
      'src/**/*.{js,vue}',
      '!src/App.vue',
      '!src/main.js',
      '!src/components/TopBar.vue',
      '!src/components/ProductList.vue',
      '!src/components/Product.vue',
      '!src/components/Modal.vue',
      '!src/components/Client.vue',
      '!src/components/ClientList.vue'
    ],
    moduleFileExtensions: [
      'js',
      'jsx',
      'json',
      'vue'
    ],
    "coverageReporters": [
        "json-summary", 
        "text",
        "lcov"
    ],
    transform: {
      '^.+\\.vue$': 'vue-jest',
      '^.+\\.jsx?$': 'babel-jest'
    },
    moduleNameMapper: {
      '^@/(.*)$': '<rootDir>/src/$1'
    },
    "testPathIgnorePatterns": [
      "test/specs/*"
    ]
  }