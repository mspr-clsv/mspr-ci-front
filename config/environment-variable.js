const httpOrHttps = process.env.VUE_APP_HTTP_OR_HTTPS === undefined ? 'http' : process.env.VUE_APP_HTTP_OR_HTTPS;
const backendUrl = process.env.VUE_APP_BACKEND_URL === undefined ? 'localhost:8000' : process.env.VUE_APP_BACKEND_URL;
const frontUrl = 'http://localhost:8081';

module.exports = {
    backendUrl,
    httpOrHttps,
    frontUrl
}
