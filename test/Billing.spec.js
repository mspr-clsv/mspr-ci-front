import Billing from "../src/components/Billing";
import { shallowMount } from "@vue/test-utils";

describe("Billing Computed", () => {
    let cmp = shallowMount(Billing)
    it("Test on subTotal computed", () => {
        cmp.setData({
            purchaseList: [
                {
                    "id": 1,
                    "client": 1,
                    "product": {
                        "id": 1,
                        "name": "zefzef",
                        "price": "12.00"
                    },
                    "amount": 1
                }
            ]
        });
        expect(cmp.vm.subtotal).toBe(12);
    });

    it("Test TVA computed", () => {
        cmp.setData({
            purchaseList: [
                {
                    "id": 1,
                    "client": 1,
                    "product": {
                        "id": 1,
                        "name": "zefzef",
                        "price": "12.00"
                    },
                    "amount": 1
                }
            ]
        });
        let subtotal = cmp.vm.subtotal
        expect(Math.round(cmp.vm.tva)).toBe(Math.round(subtotal * 15 /100));
    })
});