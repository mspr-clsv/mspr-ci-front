const clientTest = require('./testFiles/01clientTest.spec');
const productTest = require('./testFiles/02ProductTest.spec');
const cartTest = require('./testFiles/03CartTest.spec');

clientTest.doTests()
productTest.doTests();
cartTest.doTests();