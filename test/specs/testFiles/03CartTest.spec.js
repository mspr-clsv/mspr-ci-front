const assert = require('assert')
const env = require('../../../config/environment-variable')

module.exports = 
{
    doTests : function () {
        describe('select a user to use', () => {
            it('should select a user', () => {
                browser.url(env.frontUrl+'/products');
                const listeDeroulante = $('.dropdown-toggle');
                listeDeroulante.click();
                const userSelected = $('.dropdown-item=test lastname test firstname');
                userSelected.click();
                assert.strictEqual(listeDeroulante.getText(),'test lastname test firstname')
            });
            it('should add a product to the cart', () => {
                const addBTN = $('.float-right');
                addBTN.click();
        
                const cartURL = $('=Mon panier');
                cartURL.click();
        
                const img = $('.media-object');
        
                assert.strictEqual(!!img, true)
                
            });
            // it('should delete a product from the cart', () => {
            //     const deleteBTN = $('#deleteBtn');
            //     deleteBTN.click();
            //     assert.strictEqual(!!deleteBTN, null);
            // });
        });
    }
}

