const assert = require('assert')
const env  = require('../../../config/environment-variable')

module.exports =
{
    doTests : function() {
        describe('click on the user list and add a user', () => {
            it('should redirect on the user list link', () => {
                browser.url(env.frontUrl);
                const title = $('=Liste des clients');
                title.click();
                assert.strictEqual(browser.getUrl(), env.frontUrl+'/clients');
            });
            it('should add a user', () => {
                const lastname = $('#clientLastname');
                const firstname = $('#clientFirstname');
                const valider = $('.btn-primary=Valider');
        
                lastname.setValue('test lastname');
                firstname.setValue('test firstname');
        
                valider.click()
        
                const clientFirstname = $('#firstname=test firstname');
                const clientLastname = $('#lastname=test lastname');
        
                browser.refresh();
        
                assert.strictEqual(clientLastname.getText(), 'test lastname');
                assert.strictEqual(clientFirstname.getText(), 'test firstname');
                
            });
        });
    }   
}