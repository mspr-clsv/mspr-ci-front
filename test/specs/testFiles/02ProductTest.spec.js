const assert = require('assert')
const env = require('../../../config/environment-variable')

module.exports = 
{
    doTests : function() {
        describe('click on the product list and add a product', () => {
            it('should redirect on the product list link', () => {
                browser.url(env.frontUrl);
                const title = $('=Liste des produits');
                title.click();
                assert.strictEqual(browser.getUrl(),env.frontUrl+'/products');
            });
            it('should add a product', () => {
                const pName = $('#productName');
                const pPrice = $('#productPrice');
                const valider = $('.btn-primary=Créer le produit');
        
                pName.setValue('test product');
                pPrice.setValue('12');
        
                valider.click()
        
                const productName = $('#pName=test product');
                const productPrice = $('#pPrice=12.00 €');
        
                browser.refresh();
        
                assert.strictEqual(productName.getText(), 'test product');
                assert.strictEqual(productPrice.getText(), '12.00 €');
                
            });
        });
    }
}