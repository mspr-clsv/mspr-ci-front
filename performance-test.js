import { sleep } from "k6";
import http from "k6/http";

export let options = {
  duration: "1m",
  vus: 50,
  thresholds: {
    http_req_duration: ["p(95)<1000"]
  }
};

export default function() {
  http.get("https://mspr-front.herokuapp.com/");
  sleep(3);
}
